/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @desc Controller for lp_location_fetcher
 */
public with sharing class LocationFetcherController {
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc To get suggestions from google api
     */
    @AuraEnabled
    public static string getSuggestions(String input,String stateLabel) {

        String url ='';
                 url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?'
                + 'input=' + EncodingUtil.urlEncode(input, 'UTF-8')
                + '&components=country:us'
                +  getKey();
          
        String response = getResponse(url);
        return response;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc To get details of place 
     */
    @AuraEnabled
    public static string getPlaceDetails(String placeId) {
        String url = 'https://maps.googleapis.com/maps/api/place/details/json?'
                + 'placeid=' + EncodingUtil.urlEncode(placeId, 'UTF-8')
                + getKey(); 
        String response = getResponse(url);
        return response;
    }
 
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Used to get response from google api
     */
    public static string getResponse(string strURL){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setMethod('GET');
        req.setEndpoint(strURL);
        req.setTimeout(120000);
        res = h.send(req); 
        String responseBody = res.getBody(); 
        return responseBody;
    }
 
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc To get key to pass into google api
     */
    public static string getKey(){
        string key = 'AIzaSyBuN0ELGGOJ9erWduRRHBmP3Wn7VbVapXg';
        //AIzaSyA5HJitPInqpq812zO-PzH2hmgMTDT31F8
        string output = '&key=' + key;   
        return output;
    }

}
