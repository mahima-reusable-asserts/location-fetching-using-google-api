/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @desc This component is to fetch loaction from google api
 */
import {
    LightningElement,
    track,
    api
} from 'lwc';
import getSuggestions from '@salesforce/apex/LocationFetcherController.getSuggestions';
import getPlaceDetails from '@salesforce/apex/LocationFetcherController.getPlaceDetails';

export default class Demo32LocationFetcher extends LightningElement {
    @track locations = '';
    @track location = '';

    @track predictions = [];
    @track isPrediction = false;

    delayTimer;
    showClear = false;
    handlersAdded;
    searchLocationState = 'New Mexico';

    get checkPrediction() {
        return this.predictions.length == 0 ? false : true;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method get city value
     */
    getCities(event) {
        this.predictions = [];
        if (this.delayTimer) {
            clearTimeout(this.delayTimer);
        }

        let searchTerm = event.target.value.trim();
        if (!searchTerm || searchTerm === '') {
            return;
        }

        getSuggestions({
                input: searchTerm,
                stateLabel: this.searchLocationState
            }).then(result => {
                var resp = JSON.parse(result);
                let googleAddresses = resp.predictions.filter(googleItem =>
                    !this.predictions.some(sfItem => sfItem.formattedAddress === googleItem.description)
                );

                this.predictions = googleAddresses;
            })
            .catch(error => {
                this.error = error;
                console.log('error-->' + JSON.stringify(error));
            })
            .finally(() => {
                if (this.predictions.length > 0) {
                    this.isPrediction = true;
                    if (this.template.querySelector('.address-dropdown').classList.contains('display-none')) {
                        this.template
                            .querySelector('.address-dropdown').classList.remove('display-none');
                    }
                }
            });

    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method gets city details
     */
    getCityDetails(event) {

        let selectedItem = event.currentTarget;
        let placeid = selectedItem.dataset.placeid;

        getPlaceDetails({
                placeId: placeid
            }).then(result => {
                let placeDetails = JSON.parse(result);
                this.locations = placeDetails.result.address_components;
                this.location = placeDetails.result.formatted_address;
                this.sendLocation();
            })
            .catch(error => {
                this.error = error;
                console.log('error-->' + error);
            });

        this.predictions = [];
        this.showClear = true;
    }

    @api
    setDefaultLocation(address) {
        this.location = address;
        this.showClear = true;
    }

    handleAddressBlur(event) {
        //this.predictions = [];
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method dispatches location to parent
     */
    sendLocation() {
        const selectedEvent = new CustomEvent("fetchinglocation", {
            detail: {
                locations: this.locations
            }
        });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method handles clear
     */
    @api handleClear(event) {
        this.predictions = [];
        this.isPrediction = false;
        this.location = '';
        this.locations = '';
        this.showClear = false;
        this.template
            .querySelector(".address-dropdown")
            .classList.add("display-none");
        this.dispatchEvent(new CustomEvent('clear'));
    }

    hasFocus = false;
    renderedCallback() {
        if (!this.handlersAdded) {
            this.template.addEventListener("click", (event) => {
                if (event.target.closest(".slds-prediction_container")) {
                    return;
                }

                this.template
                    .querySelector(".address-dropdown")
                    .classList.add("display-none");

            });

            this.handlersAdded = true;
        }
    }
}