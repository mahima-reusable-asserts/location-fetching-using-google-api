import {
    LightningElement,
    track
} from "lwc";

export default class Demo32LocationFetcherParent extends LightningElement {
    @track dataObj = {};
    @track isLocationFetched = false;
    @track disableFields = false;

    connectedCallback() {
        if (this.dataObj.formattedAddress && this.dataObj.formattedAddress.trim().length > 0) {
            setTimeout(() => {
                this.template.querySelector('c-demo32-location-fetcher').setDefaultLocation(this.dataObj.formattedAddress);
            }, 200);
        }
    }

    locationFetched(event) {
        this.dataObj.locationId = event.detail.locationId;
        let selectedlocation = event.detail.locations;

        selectedlocation.forEach(element => {
            switch (true) {
                case element.types.includes("postal_code"):
                    this.dataObj.postalCode = element.long_name;
                    break;
                case element.types.includes("administrative_area_level_1"):
                    this.dataObj.state = element.long_name;
                    break;
                case element.types.includes("locality"):
                    this.dataObj.city = element.long_name;
                    break;
                case element.types.includes("street_number"):
                    this.dataObj.street = element.long_name;
                    break;
                case element.types.includes("country"):
                    this.dataObj.country = element.long_name;
                    break;
                case element.types.includes("route"):
                    if (this.dataObj.street != undefined) {
                        this.dataObj.route =
                            this.dataObj.street + " " + element.long_name;
                    } else {
                        this.dataObj.route = element.long_name;
                    }
                    break;
                default:
            }
        });
        this.isLocationFetched = true;
        this.disableFields = true;
    }

    handleClear() {
        this.dataObj = {};
        this.isLocationFetched = false;
        this.disableFields = false;
    }

    handleInputChange(event) {
        if (event.target.name == 'street') {
            this.dataObj.route = event.target.value;
        } else if (event.target.name == 'city') {
            this.dataObj.city = event.target.value;
        } else if (event.target.name == 'state') {
            this.dataObj.state = event.target.value;
        } else if (event.target.name == 'zip') {
            this.dataObj.postalCode = event.target.value;
        }
    }
}