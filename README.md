### Description of the Asset : ###

* This asset is a address searching component
* Developed for use in LWC only.
* Can easily be modified to suit one's needs according to a use case.

### Purpose of the Asset : ###

* This asset is an implementation of a Google API for address.
* This asset is a very basic example of how we can search address using google api and can be refined further to support any use case irrespective of its complexity.

### In this asset ###

* This component displays all the US addresses. But this can be customised in LocationFetcherController.getSuggestions()
* This asset contains a location fetcher component that uses google api to search for address. It also contains parent component that shows how the location fetcher component be used inside any parent component.
* Once the location is searched and selected by user, the location fetcher component sends an event to parent containing all information. This information can be caught by parent and can be used as per requirement.
* Please refer to "Information Filled location fetcher.png" and "No Information Filled location fetcher.png" for screenshot of how component looks.

### Who do I talk to? ###

* Mahima Aggarwal